import * as actionTypes from '../utils/actionTypes';


//初期値
const initialAppState = {
    inputValue: 0,
    resultValue: 0,
    showingResult: false,
}


const calculator = (state = initialAppState, action) => {
    if (action.type === actionTypes.INPUT_NUMBER) {
        return {
            ...state,
            inputValue: state.inputValue * 10 + action.number,
            showingResult: false,
        };
    } else if (action.type === actionTypes.PLUS) {
        console.log(state.inputValue);
        console.log(state.resultValue);
        return {
            ...state,
            inputValue: 0,
            resultValue: state.resultValue + state.inputValue,
            showingResult: true,
        }
    } else {
        return state;
    }
};

export default calculator;