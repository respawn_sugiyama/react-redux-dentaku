// reducers/index.js は複数Reducerが存在した時にそれらを組み合わせるためのもの
import { combineReducers } from 'redux';

// 各reducerを作成したらimportする
import calculator from './calculator';

const reducer = combineReducers({
    calculator,
});

export default reducer;