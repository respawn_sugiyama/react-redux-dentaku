import React from 'react';
import { render } from 'react-dom';

// store作成に必要なモジュールをimport
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import './index.css';
// import App from './App';
import CalculatorContainer from './containers/CalculatorContainer';
import reducer from './reducers';
import * as serviceWorker from './serviceWorker';

//電卓アプリで学ぶReact/Redux入門(実装編)を実践
// https://qiita.com/nishina555/items/9ff744a897af8ed1679b

// storeの作成
const store = createStore(reducer);

render(
    // rootのタグをProviderでラップ。Providerにはstoreを渡してあげる。
    <Provider store={store}>
        <CalculatorContainer />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
