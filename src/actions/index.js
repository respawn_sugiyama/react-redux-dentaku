import * as actionTypes from '../utils/actionTypes'


// 数字ボタンを押した時のactionの定義の作成
export const onNumClick = (number) => ({
    type: actionTypes.INPUT_NUMBER,
    number,
});

export const onPlusClick = () => ({
    type: actionTypes.PLUS,
})